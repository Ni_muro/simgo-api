<?php

namespace App\Exports\laporanRekapKibAF;

use App\Models\Jurnal\Kib;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Jurnal\Kamus_kab_kota;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class laporanKibBExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithHeadingRow, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $nama_aset;

	function __construct($args){
        $this->nama_aset = $args['nama_aset'];
        $this->tahun_sekarang = date('Y')-1;
        $this->bidang_barang ="";

        $this->total_harga = 0;
	}

    public function collection()
    {
        $bidang_barang = Kib::select('bidang_barang')->distinct()->where('no_key','like','13.02%')->get();

        $this->bidang_barang = $bidang_barang;

        $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','ukuran','cc','bahan','tahun_pengadaan','baik','kb', 'rb', 'no_rangka_seri', 'no_mesin', 'nopol', 'no_bpkb', 'saldo_barang', 'satuan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', 'B')
                ->where('saldo_barang', '>', '0')
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();

        $asets = array();

        $total_harga = 0;
        foreach ($data as $value) {
            $aset = $value;
            $no_register = (string)$value['no_register'] . ' ';
            $aset['no_register'] = $no_register;

            // $aset['nama_lokasi'] = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi',$value['nomor_lokasi'])->first()->nama_lokasi;

            // if ($value['kode_64'] == '1.5.3.03.01' || $value['kode_64'] == '1.5.4.01.02') {
            //     $aset['object'] = "Hak Cipta";
            // }
            // else if($value['kode_64'] == '1.5.3.05.01' || $value['kode_64'] == '1.5.4.01.01') {
            //     $aset['object'] = "Software";
            // }

            // $explode = explode(".", $value['no_key']);
            // $aset['sub_sub_kelompok'] = $explode[5];

            $total_harga_tmp = $value["harga_total_plus_pajak_saldo"];
            $total_harga+=$total_harga_tmp;
            $this->total_harga = $total_harga;

            array_push($asets, $aset);
        }

        $export = collect($asets);
        return $export;
    }

    public function startCell(): string
    {
        return 'B3';
    }

    public function headingRow(): int
    {
        return 3;
    }

    public function headings(): array
    {

        $heading =[
            ['KODE 108','NO REGISTER','NAMA BARANG','MERK/ALAMAT','UKURAN','CC','BAHAN','TAHUN PENGADAAN','BAIK','KB', 'RB', 'NO RANGKA SERI', 'NO MESIN', 'NOPOL', 'NO BPKB', 'JUMLAH BARANG', 'SATUAN', 'HARGA TOTAL', 'KETERANGAN'],
            [
                2,3,4,5,6,7,8,9,10,
                11,12,13,14,15,16,17,18,19,20
            ]
        ];

        return $heading;
    }

    public function registerEvents(): array
    {

            return [
                AfterSheet::class => function (AfterSheet $event) {
                    $max = $event->sheet->getDelegate()->getHighestRow();
                    /////set paper
                    $event->sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
                    $event->sheet->getPageSetup()->setFitToWidth(1);
                    $event->sheet->getPageSetup()->setFitToHeight(0);
                    $event->sheet->getPageSetup()->setFitToPage(true);
                    $event->sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_FOLIO);
                    $event->sheet->setShowGridlines(false);
                    $event->sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 4);

                    $event->sheet->freezePane('U5');

                    // end set paper

                    // footer
                    $event->sheet->getHeaderFooter()
                        ->setOddFooter('&L&B '. $this->nama_aset.' /'.$this->tahun_sekarang. '&R &P / &N');
                    // end footer

                    ////////////////Border
                    $event->sheet->getStyle('A3:T4')->applyFromArray([
                        'font' => [
                            'bold' => true
                        ],
                        'borders' => [
                            'top' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                'color' => ['argb' => '000000']
                            ],
                            'bottom' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                'color' => ['argb' => '000000']
                            ],
                        ],
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        ],
                    ]);
                    $event->sheet->getStyle('A5:T'.$max)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000']
                            ],
                        ],
                        'alignment' => [
                            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        ],
                    ]);
                    //////////////endborder

                    // format text
                    $event->sheet->getStyle('S4')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_TEXT);
                    // end format text

                    //////////////centering
                    $event->sheet->getStyle('F5:R'.$max)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);
                    //////end centering

                    ////////////////numbering
                    // A3
                    $event->sheet->getDelegate()->setCellValue("A3", "No.");
                    $event->sheet->getStyle('A3')->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        ]
                    ]);
                    // A4
                    $event->sheet->getDelegate()->setCellValue("A4", "1");
                    // nomor
                    $nomor = 1;
                    for($i=5;$i<=$max;$i++){
                        $event->sheet->getDelegate()->setCellValue("A".$i, $nomor);
                        $event->sheet->getStyle('A'.$i)->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            ]
                        ]);
                        $nomor++;
                    }
                    ////////////end numbering

                    ////////column width
                    //////////column B
                    $event->sheet->getColumnDimension('B')->setAutoSize(false);
                    $event->sheet->getColumnDimension('B')->setWidth(18);
                    $event->sheet->getStyle('B1:B'.$max)->getAlignment()->setWrapText(true);
                    //////////column C
                    $event->sheet->getColumnDimension('C')->setAutoSize(false);
                    $event->sheet->getColumnDimension('C')->setWidth(25);
                    $event->sheet->getStyle('C1:C'.$max)->getAlignment()->setWrapText(true);
                    //////////column D
                    $event->sheet->getColumnDimension('D')->setAutoSize(false);
                    $event->sheet->getColumnDimension('D')->setWidth(20);
                    $event->sheet->getStyle('D1:D'.$max)->getAlignment()->setWrapText(true);
                    //////////column E
                    $event->sheet->getColumnDimension('E')->setAutoSize(false);
                    $event->sheet->getColumnDimension('E')->setWidth(20);
                    $event->sheet->getStyle('E1:E'.$max)->getAlignment()->setWrapText(true);
                    //////////column F
                    $event->sheet->getColumnDimension('F')->setAutoSize(false);
                    $event->sheet->getColumnDimension('F')->setWidth(15);
                    $event->sheet->getStyle('F1:F'.$max)->getAlignment()->setWrapText(true);
                    //////////column G
                    $event->sheet->getColumnDimension('G')->setAutoSize(false);
                    $event->sheet->getColumnDimension('G')->setWidth(10);
                    $event->sheet->getStyle('G1:G'.$max)->getAlignment()->setWrapText(true);
                    //////////column H
                    $event->sheet->getColumnDimension('H')->setAutoSize(false);
                    $event->sheet->getColumnDimension('H')->setWidth(15);
                    $event->sheet->getStyle('H1:H'.$max)->getAlignment()->setWrapText(true);
                    //////////column I
                    $event->sheet->getColumnDimension('I')->setAutoSize(false);
                    $event->sheet->getColumnDimension('I')->setWidth(20);
                    $event->sheet->getStyle('I1:I'.$max)->getAlignment()->setWrapText(true);
                    //////////column J
                    $event->sheet->getColumnDimension('J')->setAutoSize(false);
                    $event->sheet->getColumnDimension('J')->setWidth(10);
                    $event->sheet->getStyle('J1:J'.$max)->getAlignment()->setWrapText(true);
                    //////////column K
                    $event->sheet->getColumnDimension('K')->setAutoSize(false);
                    $event->sheet->getColumnDimension('K')->setWidth(10);
                    $event->sheet->getStyle('K1:K'.$max)->getAlignment()->setWrapText(true);
                    //////////column L
                    $event->sheet->getColumnDimension('L')->setAutoSize(false);
                    $event->sheet->getColumnDimension('L')->setWidth(10);
                    $event->sheet->getStyle('L1:L'.$max)->getAlignment()->setWrapText(true);
                    //////////column M
                    $event->sheet->getColumnDimension('M')->setAutoSize(false);
                    $event->sheet->getColumnDimension('M')->setWidth(15);
                    $event->sheet->getStyle('M1:M'.$max)->getAlignment()->setWrapText(true);
                    //////////column N
                    $event->sheet->getColumnDimension('N')->setAutoSize(false);
                    $event->sheet->getColumnDimension('N')->setWidth(15);
                    $event->sheet->getStyle('N1:N'.$max)->getAlignment()->setWrapText(true);
                    //////////column O
                    $event->sheet->getColumnDimension('O')->setAutoSize(false);
                    $event->sheet->getColumnDimension('O')->setWidth(15);
                    $event->sheet->getStyle('O1:O'.$max)->getAlignment()->setWrapText(true);
                    //////////column P
                    $event->sheet->getColumnDimension('P')->setAutoSize(false);
                    $event->sheet->getColumnDimension('P')->setWidth(15);
                    $event->sheet->getStyle('P1:P'.$max)->getAlignment()->setWrapText(true);
                    //////////column Q
                    $event->sheet->getColumnDimension('Q')->setAutoSize(false);
                    $event->sheet->getColumnDimension('Q')->setWidth(15);
                    $event->sheet->getStyle('Q1:Q'.$max)->getAlignment()->setWrapText(true);
                    //////////column R
                    $event->sheet->getColumnDimension('R')->setAutoSize(false);
                    $event->sheet->getColumnDimension('R')->setWidth(15);
                    $event->sheet->getStyle('R1:R'.$max)->getAlignment()->setWrapText(true);
                    //////////column S
                    $event->sheet->getColumnDimension('S')->setAutoSize(true);
                    $event->sheet->getStyle('S1:S'.$max)->getAlignment()->setWrapText(true);
                    //////////column T
                    $event->sheet->getColumnDimension('T')->setAutoSize(false)->setWidth(30);
                    $event->sheet->getStyle('T1:T'.$max)->getAlignment()->setWrapText(true);
                    ///////////end column


                    /////header
                    $event->sheet->getStyle('A3:T3')->applyFromArray([
                        'font' => [
                            'bold' => true
                        ],
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                'color' => ['argb' => '000000']
                            ],
                        ],
                    ]);
                    $event->sheet->getDelegate()->mergeCells('A1:T1');
                    $event->sheet->getDelegate()->setCellValue("A1", "Laporan ".$this->nama_aset ." ".$this->tahun_sekarang);
                    $event->sheet->getStyle('A1')->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'bold' => true,
                            'size' => 18
                        ]
                    ]);
                    /////end header

                    ///////////////border total
                    $f2 = $max+1;
                    $event->sheet->getStyle('A'.$f2.':T'.$f2)->applyFromArray([
                        'borders' => [
                            'bottom' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                'color' => ['argb' => '000000']
                            ],
                        ],
                    ]);
                    $event->sheet->getDelegate()->mergeCells("Q".$f2.":R".$f2);
                    $event->sheet->getDelegate()->setCellValue('Q'.$f2, "Total");
                    $event->sheet->getStyle('Q'.$f2)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'bold' => true,
                        ]
                    ]);

                    $event->sheet->getDelegate()->setCellValue('S'.$f2 , $this->total_harga);
                    $event->sheet->getStyle('S'.$f2)->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE);
                    $event->sheet->getStyle('S'.$f2)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'bold' => true,
                        ]
                    ]);
                    ////end total

                    $date = date('d/m/Y');
                    $f1 = $max+3;
                    for($i = 0; $i<5; $i++) {
                        $event->sheet->getDelegate()->mergeCells('A'.$f1.':G'.$f1);
                        $event->sheet->getDelegate()->mergeCells('H'.$f1.':L'.$f1);
                        $event->sheet->getDelegate()->mergeCells('M'.$f1.':T'.$f1);
                        $event->sheet->getStyle('A'.$f1)->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            ],
                        ]);
                        $event->sheet->getStyle('M'.$f1)->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            ]
                        ]);

                        if($i == 0) {
                            $event->sheet->getDelegate()->setCellValue('A'.$f1, "Mengetahui");
                            $event->sheet->getDelegate()->setCellValue('M'.$f1, "Mojokerto, ".$date);
                        }

                        if($i == 4) {
                            $event->sheet->getDelegate()->setCellValue('A'.$f1, "NIP");
                            $event->sheet->getDelegate()->setCellValue('M'.$f1, "NIP");
                        }

                        $f1++;
                    }
                },
            ];
    }

    public function columnFormats(): array
    {
            return [
                'C' => NumberFormat::FORMAT_TEXT,
                'S' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE
            ];
    }

    public function title(): string
    {
        return 'Kib B';
    }
}
