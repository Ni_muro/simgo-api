<?php

namespace App\Exports;

use App\Models\Jurnal\Kib;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Jurnal\Kamus_kab_kota;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use App\Exports\laporanRekapKibAF\laporanKibAExport;
use App\Exports\laporanRekapKibAF\laporanKibBExport;
use App\Exports\laporanRekapKibAF\laporanKibCExport;
use App\Exports\laporanRekapKibAF\laporanKibDExport;
use App\Exports\laporanRekapKibAF\laporanKibEExport;
use App\Exports\laporanRekapKibAF\laporanKibFExport;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class laporanKibAFExport implements WithMultipleSheets
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $nama_aset;

	function __construct($args){
        $this->nama_aset = $args['nama_aset'];
        $this->tahun_sekarang = date('Y')-1;
        $this->bidang_barang ="";

        $this->total_harga = 0;
	}

    public function sheets(): array
    {
        $data = array();
        $data["nama_aset"] = $this->nama_aset;

        return [
            0 => new laporanKibAExport($data),
            1 => new laporanKibBExport($data),
            2 => new laporanKibCExport($data),
            3 => new laporanKibDExport($data),
            4 => new laporanKibEExport($data),
            5 => new laporanKibFExport($data),
        ];
    }
}
