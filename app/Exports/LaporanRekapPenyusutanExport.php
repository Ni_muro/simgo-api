<?php

namespace App\Exports;

use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Rehab;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Kamus_sub_unit;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\Kamus\Sub_sub_rincian_108;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class LaporanRekapPenyusutanExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithHeadingRow, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $nomor_lokasi;
    public $kode_kepemilikan;
    public $bidang_barang;
    public $jenis_aset;

    function __construct($args){
        $this->nama_jurnal = $args['nama_jurnal'];

        $this->tahun_sekarang = date('Y')-1;

        $this->total_kib_a = 0;
        $this->total_kib_b = 0;
        $this->total_kib_c = 0;
        $this->total_kib_d = 0;
        $this->total_kib_e = 0;
        $this->total_kib_f = 0;
        $this->total_kib_g = 0;
        $this->total_kib_r = 0;
    }

    public function collection()
    {
        ini_set('max_execution_time', 1800);
        $rekap = array();
        $i = 0;

        $daftar_sub_unit = Kamus_sub_unit::select('nomor_sub_unit', 'nama_sub_unit')->get();

        foreach ($daftar_sub_unit as $unit) {
            $daftar_kib = array('1.3.1', '1.3.2', '1.3.3', '1.3.4', '.1.3.5', '1.3.6', '1.5.3', '1.5.4');
            $penyusutan_kib_a = 0;
            $penyusutan_kib_b = 0;
            $penyusutan_kib_c = 0;
            $penyusutan_kib_d = 0;
            $penyusutan_kib_e = 0;
            $penyusutan_kib_f = 0;
            $penyusutan_kib_g = 0;
            $penyusutan_kib_r = 0;

            foreach ($daftar_kib as $kib) {
                $data = Kib::where('nomor_lokasi', 'like', $unit['nomor_sub_unit'] . '%')
                ->where('kode_108', 'like', $kib . '%')
                ->where('saldo_barang', '>', 0)
                ->get()
                ->toArray();

                $j = 0;
                $jumlah_barang = 0;
                $tahun_acuan = date('Y')-1;
                $masa_terpakai;
                $total_nilai_perolehan = 0;
                $total_akumulasi_penyusutan = 0;
                $total_akumulasi_penyusutan_berjalan = 0;
                $total_beban = 0;
                $total_nilai_buku = 0;
                $total_rehab = 0;
                $jumlah_rehab = 0;

                foreach($data as $value) {
                    if(array_key_exists('kode_64', $value)) {
                        if(is_null($value['kode_64'])) {
                            $mm = null;
                        } else {
                            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
                        }
                    } else {
                        $mm = NULL;
                    }

                    $saldo_kosong = false;
                    $aset_induk = false;
                    $aset_rehab = false;
                    $kib_e = false;

                    if($value["saldo_barang"] == 0) {
                        $saldo_kosong = true;
                    }

                    $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
                    $sub_64 = substr($value["kode_64"], 0, 8);

                    if(in_array($sub_64, $kode_e)) {
                        if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                            $kib_e = false;
                        } else {
                            $kib_e = true;
                        }
                    }

                    $pakai_habis = false;
                    $ekstrakom = false;

                    if($value["pos_entri"] == "PAKAI_HABIS") {
                        $pakai_habis = true;
                    }

                    if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                        $ekstrakom = true;
                    }

                    $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
                    $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

                    if(!is_null($induk)) {
                        $aset_induk = true;
                    }

                    if(!is_null($anak)) {
                        $aset_rehab = true;
                    }

                    if($saldo_kosong || $aset_rehab || $pakai_habis || $ekstrakom) {
                        continue;
                    }

                    if(!is_null($mm)) {
                        if($mm->masa_manfaat == 0) {
                            $masa_manfaat = 0;
                            $mm_induk = 0;
                        } else {
                            $masa_manfaat = (int)$mm->masa_manfaat;
                            $mm_induk = (int)$mm->masa_manfaat;
                        }
                    } else {
                        if($this->bidang_barang == "A") {
                            $masa_manfaat = 0;
                            $mm_induk = 0;
                        } else if($this->bidang_barang == "B") {
                            $masa_manfaat = 5;
                            $mm_induk = 5;
                        } else if($this->bidang_barang == "C" || $this->bidang_barang == "D") {
                            $masa_manfaat = 50;
                            $mm_induk = 50;
                        } else if($this->bidang_barang == "G") {
                            $masa_manfaat = 4;
                            $mm_induk = 4;
                        } else if(strpos($this->bidang_barang, 'E') !== false) {
                            $masa_manfaat = 5;
                            $mm_induk = 5;
                        }
                    }

                    if($masa_manfaat == 0) {
                        if($value['kode_108'] == '1.5.4.01.01.01.002') {
                            $masa_manfaat = 5;
                            $mm_induk = 5;
                        } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                            $masa_manfaat = 50;
                            $mm_induk = 50;
                        } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                            $masa_manfaat = 40;
                            $mm_induk = 40;
                        } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                            $masa_manfaat = 5;
                            $mm_induk = 5;
                        }
                    }

                    if($aset_induk) {
                        $rehabs = Rehab::join('kibs', 'kibs.id_aset', '=', 'rehabs.rehab_id')
                        ->select('rehabs.rehab_id', 'kibs.tahun_pengadaan as tahun_rehab', 'kibs.harga_total_plus_pajak_saldo as nilai_rehab', 'kibs.kode_108 as kode_rek_rehab', 'rehabs.tambah_manfaat')
                        ->where('rehabs.aset_induk_id', $value['id_aset'])
                        ->orderBy('kibs.tahun_pengadaan', 'asc')->get();


                        if(!empty($rehabs)) {
                            $detail_penyusutan = $rehabs->toArray();
                        }

                        $count = sizeof($detail_penyusutan);

                        $status_aset = 1;

                        $jumlah_barang_tmp = $value["saldo_barang"];
                        $tahun_pengadaan_tmp = intval($value["tahun_pengadaan"]);
                        $nilai_pengadaan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                        $persen = 0;
                        $penambahan_nilai = 0;
                        $masa_tambahan = 0;
                        $masa_terpakai_tmp = 0;
                        $sisa_masa_tmp = $masa_manfaat;
                        $penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
                        $akumulasi_penyusutan_tmp = 0;
                        $akumulasi_penyusutan_berjalan_tmp = 0;
                        $nilai_buku_tmp = 0;
                        $index = 0;
                        $k = 0;

                        for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
                            $tahun_rehab = intval($detail_penyusutan[$index]["tahun_rehab"]);
                            if($th == $tahun_rehab) {
                                $jumlah_renov_tahunx = 0;
                                $tahun_pembanding = 0;

                                if($tahun_pengadaan_tmp == $tahun_rehab) {
                                    $nilai_buku_tmp = $nilai_pengadaan_tmp;
                                }

                                for ($x = 0; $x < $count; $x++) {
                                     $tahun_pembanding = intval($detail_penyusutan[$x]["tahun_rehab"]);

                                     if($tahun_pembanding === $tahun_rehab) {
                                        $jumlah_renov_tahunx++;
                                     }
                                }

                                if($jumlah_renov_tahunx == 1) {
                                    $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;

                                    $persen = (int)$persen;

                                    $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                    if(is_null($kode_64)) {
                                        $kode_108 = substr($kode_108, 0, 14);
                                        $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                        if(is_null($kode_64)) {
                                            $kode_108 = substr($kode_108, 0, 11);
                                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                        } else {
                                            $kode_64 = $kode_64->kode_64;
                                        }
                                    } else {
                                        $kode_64 = $kode_64->kode_64;
                                    }

                                    $kode_64 = substr($kode_64, 0, 8);
                                    $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                                    if(!is_null($masa_tambah)) {
                                        $masa_tambahan = $masa_tambah->masa_tambahan;
                                    }

                                    if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                        $sisa_masa_tmp += $masa_tambahan;
                                        if($sisa_masa_tmp > $mm_induk) {
                                            $masa_manfaat = $mm_induk;
                                            $sisa_masa_tmp = $mm_induk;
                                        } else {
                                            $masa_manfaat = $sisa_masa_tmp;
                                        }
                                        $masa_terpakai_tmp = 1;
                                    } else {
                                        ++$masa_terpakai_tmp;
                                    }

                                    $nilai_buku_tmp += $detail_penyusutan[$index]["nilai_rehab"];

                                    if($sisa_masa_tmp > 0) {
                                        $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                                    } else {
                                        $penyusutan_per_tahun_tmp = 0;
                                    }

                                    $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                                    $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                                    --$sisa_masa_tmp;
                                    $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilai_rehab"];
                                    $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                                    if($index < $count-1) {
                                        $index++;
                                    }
                                } else {
                                    $penambahan_nilai = 0;
                                    for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
                                        $penambahan_nilai += $detail_penyusutan[$index]["nilai_rehab"];
                                        if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
                                            $index++;
                                        }
                                    }

                                    $persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;

                                    $persen = (int)$persen;
                                    $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                    if(is_null($kode_64)) {
                                        $kode_108 = substr($kode_108, 0, 14);
                                        $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                        if(is_null($kode_64)) {
                                            $kode_108 = substr($kode_108, 0, 11);
                                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                        } else {
                                            $kode_64 = $kode_64->kode_64;
                                        }
                                    } else {
                                        $kode_64 = $kode_64->kode_64;
                                    }

                                    $kode_64 = substr($kode_64, 0, 8);
                                    $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                                    if(!is_null($masa_tambah)) {
                                        $masa_tambahan = $masa_tambah->masa_tambahan;
                                    }

                                    if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                        $sisa_masa_tmp += $masa_tambahan;
                                        if($sisa_masa_tmp > $mm_induk) {
                                            $masa_manfaat = $mm_induk;
                                            $sisa_masa_tmp = $mm_induk;
                                        } else {
                                            $masa_manfaat = $sisa_masa_tmp;
                                        }
                                        $masa_terpakai_tmp = 1;
                                    } else {
                                        ++ $masa_terpakai_tmp;
                                    }

                                    $nilai_buku_tmp += $penambahan_nilai;
                                    $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                                    $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                                    $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                                    $nilai_pengadaan_tmp += $penambahan_nilai;

                                    $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;
                                    if($sisa_masa_tmp > 0) {
                                        --$sisa_masa_tmp;
                                    }

                                    if($index < $count-1) {
                                        $index++;
                                    }
                                }
                            } else {
                                ++$masa_terpakai_tmp;

                                if($sisa_masa_tmp > 0) {
                                    --$sisa_masa_tmp;
                                }

                                $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                                $nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;
                                $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                                if($masa_terpakai_tmp > $masa_manfaat) {
                                    $akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
                                    $akumulasi_penyusutan_berjalan_tmp = $nilai_pengadaan_tmp;
                                    $nilai_buku_tmp = 0;
                                    $sisa_masa_tmp = 0;
                                }
                            }
                        }

                        $akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;
                        $jumlah_barang += $jumlah_barang_tmp;

                        if($this->bidang_barang == "A" || $kib_e == true) {
                            $akumulasi_penyusutan_tmp = 0;
                            $akumulasi_penyusutan_berjalan_tmp = 0;
                            $penyusutan_per_tahun_tmp = 0;
                            $nilai_buku_tmp = $nilai_pengadaan_tmp;
                            $akumulasi_penyusutan = 0;
                            $beban = 0;
                            $nilai_buku = $nilai_pengadaan_tmp;
                        }

                        if($kib == '1.3.1') {
                            $penyusutan_kib_a += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.2') {
                            $penyusutan_kib_b += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.3') {
                            $penyusutan_kib_c += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.4') {
                            $penyusutan_kib_d += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.5') {
                            $penyusutan_kib_e += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.6') {
                            $penyusutan_kib_f += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.5.3') {
                            $penyusutan_kib_g += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.5.4') {
                            $penyusutan_kib_r += $akumulasi_penyusutan_berjalan_tmp;
                        }
                    } else {
                        $masa_terpakai = $tahun_acuan - $value["tahun_pengadaan"] + 1;
                        $masa_sisa = $masa_manfaat - $masa_terpakai;
                        $nilai_perolehan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                        $status_aset = 0;
                        $jumlah_barang_tmp = $value["saldo_barang"];

                        if($masa_terpakai > $masa_manfaat) {
                            $nilai_perolehan = $nilai_perolehan_tmp;
                            $akumulasi_penyusutan = $nilai_perolehan_tmp;
                            $akumulasi_penyusutan_berjalan = $nilai_perolehan_tmp;
                            $masa_sisa = 0;
                            $beban = 0;
                            $nilai_buku = 0;
                        } else {
                            $nilai_perolehan = $nilai_perolehan_tmp;
                            $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
                            $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan_tmp;
                            $beban = 1/$masa_manfaat*$nilai_perolehan_tmp;
                            $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
                        }

                        if($this->bidang_barang == "A" || $kib_e == true) {
                            $akumulasi_penyusutan = 0;
                            $akumulasi_penyusutan_berjalan = 0;
                            $beban = 0;
                            $nilai_buku = $nilai_perolehan_tmp;
                        }

                        if($kib == '1.3.1') {
                            $penyusutan_kib_a += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.2') {
                            $penyusutan_kib_b += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.3') {
                            $penyusutan_kib_c += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.4') {
                            $penyusutan_kib_d += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.5') {
                            $penyusutan_kib_e += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.6') {
                            $penyusutan_kib_f += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.5.3') {
                            $penyusutan_kib_g += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.5.4') {
                            $penyusutan_kib_r += $akumulasi_penyusutan_berjalan;
                        }
                    }

                }


                if($kib == '1.3.1') {
                    $this->total_kib_a += $penyusutan_kib_a;
                } else if($kib == '1.3.2') {
                    $this->total_kib_b += $penyusutan_kib_b;
                } else if($kib == '1.3.3') {
                    $this->total_kib_c += $penyusutan_kib_c;
                } else if($kib == '1.3.4') {
                    $this->total_kib_d += $penyusutan_kib_d;
                } else if($kib == '1.3.5') {
                    $this->total_kib_e += $penyusutan_kib_e;
                } else if($kib == '1.3.6') {
                    $this->total_kib_f += $penyusutan_kib_f;
                } else if($kib == '1.5.3') {
                    $this->total_kib_g += $penyusutan_kib_g;
                } else if($kib == '1.5.4') {
                    $this->total_kib_r += $penyusutan_kib_r;
                }
            }

            $rekap[$i++] = array(
                'nama_sub_unit' => $unit['nama_sub_unit'],
                'penyusutan_kib_a' => $penyusutan_kib_a,
                'penyusutan_kib_b' => $penyusutan_kib_b,
                'penyusutan_kib_c' => $penyusutan_kib_c,
                'penyusutan_kib_d' => $penyusutan_kib_d,
                'penyusutan_kib_e' => $penyusutan_kib_e,
                'penyusutan_kib_f' => $penyusutan_kib_f,
                'penyusutan_kib_g' => $penyusutan_kib_g,
                'penyusutan_kib_r' => $penyusutan_kib_r
            );
        }

        // var_dump($rekap);
        // die;

        $export = collect($rekap);
        return $export;
    }

    public function startCell(): string
    {
        return 'B2';
    }

    public function headingRow(): int
    {
        return 2;
    }

    public function headings(): array
    {
        $heading = [
            ['SKPD', 'Penyusutan KIB A', 'Penyusutan KIB B', 'Penyusutan KIB C', 'Penyusutan KIB D', 'Penyusutan KIB E', 'Penyusutan KIB F', 'Penyusutan KIB G', 'Penyusutan KIB R'],
            [
                2,3,4,5,6,7,8,9,10
            ]
        ];

        return $heading;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $max = $event->sheet->getDelegate()->getHighestRow();
                /////set paper
                $event->sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getPageSetup()->setFitToWidth(1);
                $event->sheet->getPageSetup()->setFitToHeight(0);
                $event->sheet->getPageSetup()->setFitToPage(true);
                $event->sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_FOLIO);
                $event->sheet->setShowGridlines(false);
                $event->sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2, 3);

                $event->sheet->freezePane('K4');

                // end set paper

                // footer
                $event->sheet->getHeaderFooter()
                    ->setOddFooter('&L&B '. $this->nama_jurnal.' / '.$this->tahun_sekarang. '&R &P / &N');
                // end footer

                ////////////////Border
                $event->sheet->getStyle('A2:J2')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A3:J3')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A4:J'.$max)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                //////////////endborder

                // format text
                $event->sheet->getStyle('C3:J3')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_TEXT);
                // end format text

                ////////////////numbering
                // A2
                $event->sheet->getDelegate()->setCellValue("A2", "No.");
                $event->sheet->getStyle('A2')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ]);
                // A3
                $event->sheet->getDelegate()->setCellValue("A3", "1");
                // nomor
                $nomor = 1;
                for($i=4;$i<=$max;$i++){
                    $event->sheet->getDelegate()->setCellValue("A".$i, $nomor);
                    $event->sheet->getStyle('A'.$i)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ]
                    ]);
                    $nomor++;
                }
                ////////////end numbering

                ////////column width
                //////////column B
                $event->sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(25);
                $event->sheet->getStyle('B1:B'.$max)->getAlignment()->setWrapText(true);
                //////////column C
                $event->sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('C1:C'.$max)->getAlignment()->setWrapText(true);
                //////////column D
                $event->sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('D1:D'.$max)->getAlignment()->setWrapText(true);
                //////////column E
                $event->sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('E1:E'.$max)->getAlignment()->setWrapText(true);
                //////////column F
                $event->sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('F1:F'.$max)->getAlignment()->setWrapText(true);
                //////////column G
                $event->sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('G1:G'.$max)->getAlignment()->setWrapText(true);
                //////////column H
                $event->sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('H1:H'.$max)->getAlignment()->setWrapText(true);
                //////////column I
                $event->sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('I1:I'.$max)->getAlignment()->setWrapText(true);
                //////////column J
                $event->sheet->getColumnDimension('J')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('J1:J'.$max)->getAlignment()->setWrapText(true);
                ///////////end column


                /////header
                $event->sheet->getStyle('A1:J1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                ]);
                $event->sheet->getDelegate()->mergeCells('A1:J1');
                $event->sheet->getDelegate()->setCellValue("A1", "Laporan ".$this->nama_jurnal ." ".$this->tahun_sekarang);
                $event->sheet->getStyle('A1')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 18
                    ]
                ]);
                /////end header

                ///////////////border total
                $f2 = $max+1;
                $event->sheet->getStyle('A'.$f2.':J'.$f2)->applyFromArray([
                    'borders' => [
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                ]);
                $event->sheet->getDelegate()->setCellValue('B'.$f2, "Total");
                $event->sheet->getStyle('B'.$f2)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                    ]
                ]);

                $event->sheet->getDelegate()->setCellValue('C'.$f2 , $this->total_kib_a);
                $event->sheet->getDelegate()->setCellValue('D'.$f2 , $this->total_kib_b);
                $event->sheet->getDelegate()->setCellValue('E'.$f2 , $this->total_kib_c);
                $event->sheet->getDelegate()->setCellValue('F'.$f2 , $this->total_kib_d);
                $event->sheet->getDelegate()->setCellValue('G'.$f2 , $this->total_kib_e);
                $event->sheet->getDelegate()->setCellValue('H'.$f2 , $this->total_kib_f);
                $event->sheet->getDelegate()->setCellValue('I'.$f2 , $this->total_kib_g);
                $event->sheet->getDelegate()->setCellValue('J'.$f2 , $this->total_kib_r);
                $event->sheet->getStyle('C'.$f2.':J'.$f2)->getNumberFormat()
                    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE);
                $event->sheet->getStyle('C'.$f2.':J'.$f2)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                    ]
                ]);
                ////end total

                $date = date('d/m/Y');
                $f1 = $max+3;
                for($i = 0; $i<5; $i++) {
                    $event->sheet->getDelegate()->mergeCells('A'.$f1.':D'.$f1);
                    $event->sheet->getDelegate()->mergeCells('G'.$f1.':J'.$f1);
                    $event->sheet->getStyle('A'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);
                    $event->sheet->getStyle('G'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ]
                    ]);

                    if($i == 0) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "Mengetahui");
                        $event->sheet->getDelegate()->setCellValue('G'.$f1, "Mojokerto, ".$date);
                    }

                    if($i == 4) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "NIP");
                        $event->sheet->getDelegate()->setCellValue('G'.$f1, "NIP");
                    }

                    $f1++;
                }
            },
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'D' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'E' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'F' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'H' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'I' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'J' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE
        ];
    }

    public function title(): string
    {
        return 'Data rekap Penyusutan';
    }

}
