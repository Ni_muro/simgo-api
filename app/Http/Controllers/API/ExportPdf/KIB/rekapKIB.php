<?php

namespace App\Http\Controllers\API\ExportPdf\KIB;
use PDF;
use MPDF;
use Validator;
use App\Models\Jurnal\Kib;
use App\Models\jurnal\Rehab;
use Illuminate\Http\Request;
use App\Models\Jurnal\Kib_awal;
use App\Models\Kamus\Kamus_unit;
use App\Models\Kamus\Kamus_kab_kota;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Kamus_sub_unit;
use App\Models\Kamus\Sub_rincian_108;
use App\Http\Controllers\API\BaseController as BaseController;

class rekapKib extends BaseController
{
    public $nomor_lokasi;
    public $kode_kepemilikan;
    public $bidang_barang;

    public function __construct()
    {
        $this->total_nilai = 0;
        $this->tahun_sekarang = date('Y')-1;
    }

    public function exportLaporanKibUrutTahun($bidang_barang, $nomor_lokasi, $kode_kepemilikan)
    {
        if($bidang_barang == "A") {
            $data = Kib::select('kode_108', 'no_register', 'nama_barang', 'merk_alamat', 'luas_tanah', 'tahun_pengadaan', 'tgl_sertifikat', 'no_sertifikat', 'penggunaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.1%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "B") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','ukuran','cc','bahan','tahun_pengadaan','baik','kb', 'rb', 'no_rangka_seri', 'no_mesin', 'nopol', 'no_bpkb', 'saldo_barang', 'satuan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.2%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "C") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','jumlah_lantai','luas_lantai','no_imb', 'tgl_imb', 'luas_bangunan', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.3%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "D") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','panjang_tanah','lebar_tanah','luas_tanah','no_imb', 'tgl_imb', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.4%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "E") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','jumlah_lantai','luas_lantai','no_imb', 'tgl_imb', 'luas_bangunan', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.5%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')->get()
                ->toArray();
        } else if($bidang_barang == "F") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','jumlah_lantai','luas_lantai','no_imb', 'tgl_imb', 'luas_bangunan', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('kode_108', 'like', '1.3.6%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "G") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','jumlah_lantai','luas_lantai','no_imb', 'tgl_imb', 'luas_bangunan', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.5.3%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "RB") {
            if($nomor_lokasi == '12.01.35.16.445.00001.00001') {
                $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','ukuran','cc','bahan','tahun_pengadaan','baik','kb', 'rb', 'no_rangka_seri', 'no_mesin', 'nopol', 'no_bpkb', 'saldo_barang', 'satuan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('kode_108', 'like', '1.5.4%')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
            } else {
                $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','ukuran','cc','bahan','tahun_pengadaan','baik','kb', 'rb', 'no_rangka_seri', 'no_mesin', 'nopol', 'no_bpkb', 'perolehan_pemda', 'saldo_barang', 'satuan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('kode_108', 'like', '1.5.4%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
            }

        }else {
            $data = Kib::select('kode_108', 'no_register', 'nama_barang', 'merk_alamat', 'luas_tanah', 'tahun_pengadaan', 'tgl_sertifikat', 'no_sertifikat', 'penggunaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.1%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        }

        $asets = array();
        foreach ($data as $value) {
            $aset = $value;
            $no_register = (string)$value['no_register'] . ' ';
            $aset['no_register'] = $no_register;

            array_push($asets, $aset);
        }

        $data = collect($asets);
        // print_r($data->toArray());

        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data->toArray();

        $nama_file = "Laporan KIB " . $bidang_barang . " " . $nama_lokasi." ".$this->tahun_sekarang ;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.laporanKib', ['data'=>$data, 'nama_lokasi'=>$nama_lokasi, 'bidang_barang'=>$bidang_barang]);

        return $pdf->stream($nama_file.$format_file);

        // return $data;
    }

    public function exportKib64($nomor_lokasi, $kode_kepemilikan)
    {
        $data = Kib::where('nomor_lokasi', 'like', $nomor_lokasi . '%')->where('kode_kepemilikan', $kode_kepemilikan)->get();

        $rekap_64 = array();

        // loop khusus build map 64
        foreach($data as $value) {
            $kode_64 = $value["kode_64"];

            if($kode_64 != '' || !is_null($kode_64) || $kode_64 != 0) {
                $uraian = Kamus_rekening::select("uraian_64")->where('kode_64', $kode_64)->first();
            }

            if(!is_null($uraian)) {
                $uraian_64 = $uraian->uraian_64;
            }

            $found = false;
            foreach($rekap_64 as $key => $value)
            {
                if ($value["kode_64"] == $kode_64) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_64)) {
                    array_push($rekap_64, array(
                        "kode_64" => 0,
                        "uraian_64" => 0,
                        "nilai" => 0
                    ));
                } else {
                    array_push($rekap_64, array(
                        "kode_64" => $kode_64,
                        "uraian_64" => $uraian_64,
                        "nilai" => 0
                    ));
                }
            }
        }

        //loop khusus penjumlahan data
        foreach($data as $value) {
            $kode_64 = $value["kode_64"];
            $this->total_nilai += $value["harga_total_plus_pajak_saldo"];

            foreach($rekap_64 as $key => $rekap)
            {
                if($kode_64 != '' || !is_null($kode_64) || $kode_64 != 0) {
                    if ($rekap["kode_64"] == $kode_64) {
                        $rekap_64[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                        $found = true;
                        break;
                    }
                } else {
                    $rekap_64[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                    $found = true;
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_64, 'kode_64'), SORT_ASC, $rekap_64);

        $data = collect($rekap_64);

        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }

        $data->toArray();

        $nama_file = "Rekap KIB 64 " . $nama_lokasi." ".$this->tahun_sekarang;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.RekapKib64', ['data'=>$data, 'nama_lokasi'=>$nama_lokasi],[],['orientation'=>'P']);

        return $pdf->stream($nama_file.$format_file);
    }

    public function exportKib108($nomor_lokasi, $kode_kepemilikan)
    {
        $data = Kib::where('nomor_lokasi', 'like', $nomor_lokasi . '%')->where('kode_kepemilikan', $kode_kepemilikan)->get();

        $data_rehab = Rehab::where('nomor_lokasi', 'like', $nomor_lokasi . '%')->get();

        $rekap_108 = array();

        // loop khusus build map 108
        foreach($data as $value) {
            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $rehab = true;
                        break;
                    }
                }
            }

            $kode_108 = $value["kode_108"];

            if(!is_null($kode_108)) {
                $kode_108 = substr($value["kode_108"], 0, 11);
                $uraian = Rincian_108::select("uraian_rincian")->where('rincian', $kode_108)->first();
            }

            if(!is_null($uraian)) {
                $uraian_108 = $uraian->uraian_rincian;
            }

            $found = false;
            foreach($rekap_108 as $key => $value)
            {
                if ($value["kode_108"] == $kode_108) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_108)) {
                    if(!$rehab) {
                        array_push($rekap_108, array(
                            "kode_108" => 0,
                            "uraian_108" => 0,
                            "nilai" => 0
                        ));
                    }
                } else {
                    array_push($rekap_108, array(
                        "kode_108" => $kode_108,
                        "uraian_108" => $uraian_108,
                        "nilai" => 0
                    ));
                }
            }
        }

        //untuk penjumlahan data
        foreach($data as $value) {
            $kode_108 = substr($value["kode_108"], 0, 11);
            $this->total_nilai += $value["harga_total_plus_pajak_saldo"];

            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $kode_108_induk = Kib::select("kode_108")->where("id_aset", $value_rehab["aset_induk_id"])->first();
                        if(!empty($kode_108_induk)) {
                            $kode_108 = $kode_108_induk->kode_108;
                            $kode_108 = substr($value["kode_108"], 0, 11);
                            break;
                        }
                    }
                }
            }

            foreach($rekap_108 as $key => $rekap)
            {
                if($kode_108 != '' || !is_null($kode_108) || $kode_108 != 0) {
                    if ($rekap["kode_108"] == $kode_108) {
                        $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                        $found = true;
                        break;
                    }
                } else {
                    $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                    $found = true;
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_108, 'kode_108'), SORT_ASC, $rekap_108);

        $data = collect($rekap_108);

        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }

        $data->toArray();

        $nama_file = "Rekap KIB 108 " . $nama_lokasi." ".$this->tahun_sekarang;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.RekapKib108', ['data'=>$data, 'nama_lokasi'=>$nama_lokasi],[],['orientation'=>'P']);

        return $pdf->stream($nama_file.$format_file);
    }

    public function exportLaporanRekapRincian108($nomor_lokasi, $kode_kepemilikan)
    {
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '-1');
        ini_set("pcre.backtrack_limit", "1000000000");

        $data = Kib::where('nomor_lokasi', 'like', $nomor_lokasi . '%')->where('kode_kepemilikan', $kode_kepemilikan)->get();

        $data_rehab = Rehab::where('nomor_lokasi', 'like', $this->nomor_lokasi . '%')->get();

        $rekap_108 = array();

        // loop khusus build map 108
        foreach($data as $value) {
            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $rehab = true;
                        break;
                    }
                }
            }

            $kode_108 = $value["kode_108"];

            if(!is_null($kode_108)) {
                $kode_108 = substr($value["kode_108"], 0, 11);
                $uraian = Rincian_108::select("uraian_rincian")->where('rincian', $kode_108)->first();
            }

            if(!is_null($uraian)) {
                $uraian_108 = $uraian->uraian_rincian;
            }

            $found = false;
            foreach($rekap_108 as $key => $value)
            {
                if ($value["kode_108"] == $kode_108) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_108)) {
                    if(!$rehab) {
                        array_push($rekap_108, array(
                            "kode_108" => 0,
                            "uraian_108" => 0,
                            "nilai" => 0
                        ));
                    }
                } else {
                    array_push($rekap_108, array(
                        "kode_108" => $kode_108,
                        "uraian_108" => $uraian_108,
                        "nilai" => 0
                    ));
                }
            }
        }

        //untuk penjumlahan data
        foreach($data as $value) {
            $kode_108 = substr($value["kode_108"], 0, 11);
            $this->total_nilai += $value["harga_total_plus_pajak_saldo"];

            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $kode_108_induk = Kib::select("kode_108")->where("id_aset", $value_rehab["aset_induk_id"])->first();
                        if(!empty($kode_108_induk)) {
                            $kode_108 = $kode_108_induk->kode_108;
                            $kode_108 = substr($value["kode_108"], 0, 11);
                            break;
                        }
                    }
                }
            }

            foreach($rekap_108 as $key => $rekap)
            {
                if($kode_108 != '' || !is_null($kode_108) || $kode_108 != 0) {
                    if ($rekap["kode_108"] == $kode_108) {
                        $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                        $found = true;
                        break;
                    }
                } else {
                    $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                    $found = true;
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_108, 'kode_108'), SORT_ASC, $rekap_108);

        $data = collect($rekap_108);

        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }

        $data->toArray();

        $nama_file = "Laporan Rekap Barang Per Rincian Permen 108 " . $nama_lokasi." ".$this->tahun_sekarang;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.rekapRincian108', ['data'=>$data, 'nama_lokasi'=>$nama_lokasi], [],['orientation'=>'P']);

        return $pdf->stream($nama_file.$format_file);

        // return $data;

    }

    public function exportLaporanRekapSubRincian108($nomor_lokasi, $kode_kepemilikan)
    {
        $data = Kib::where('nomor_lokasi', 'like', $nomor_lokasi . '%')->where('kode_kepemilikan', $kode_kepemilikan)->get();

        $data_rehab = Rehab::where('nomor_lokasi', 'like', $this->nomor_lokasi . '%')->get();

        $rekap_108 = array();

        // loop khusus build map 108
        foreach($data as $value) {
            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $rehab = true;
                        break;
                    }
                }
            }

            $kode_108 = $value["kode_108"];

            if(!is_null($kode_108)) {
                $kode_108 = substr($value["kode_108"], 0, 14);
                $uraian = Sub_rincian_108::select("uraian_sub_rincian")->where('sub_rincian', $kode_108)->first();
            }

            if(!is_null($uraian)) {
                $uraian_108 = $uraian->uraian_sub_rincian;
            }

            $found = false;
            foreach($rekap_108 as $key => $value)
            {
                if ($value["kode_108"] == $kode_108) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_108)) {
                    if(!$rehab) {
                        array_push($rekap_108, array(
                            "kode_108" => 0,
                            "uraian_108" => 0,
                            "nilai" => 0
                        ));
                    }
                } else {
                    array_push($rekap_108, array(
                        "kode_108" => $kode_108,
                        "uraian_108" => $uraian_108,
                        "nilai" => 0
                    ));
                }
            }
        }

        foreach($data as $value) {
            $kode_108 = substr($value["kode_108"], 0, 14);
            $this->total_nilai += $value["harga_total_plus_pajak_saldo"];

            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $kode_108_induk = Kib::select("kode_108")->where("id_aset", $value_rehab["aset_induk_id"])->first();
                        if(!empty($kode_108_induk)) {
                            $kode_108 = $kode_108_induk->kode_108;
                            $kode_108 = substr($value["kode_108"], 0, 14);
                            break;
                        }
                    }
                }
            }

            foreach($rekap_108 as $key => $rekap)
            {
                if($kode_108 != '' || !is_null($kode_108) || $kode_108 != 0) {
                    if ($rekap["kode_108"] == $kode_108) {
                        $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                        $found = true;
                        break;
                    }
                } else {
                    $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                    $found = true;
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_108, 'kode_108'), SORT_ASC, $rekap_108);

        $data = collect($rekap_108);

        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }

        $data->toArray();

        $nama_file = "Laporan Rekap Barang Per Sub Rincian Permen 108 " . $nama_lokasi." ".$this->tahun_sekarang;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.rekapSubRincian108', ['data'=>$data, 'nama_lokasi'=>$nama_lokasi],[],['orientation'=> 'P']);

        return $pdf->stream($nama_file.$format_file);
    }

    public function exportLaporanKibAwal($bidang_barang, $nomor_lokasi, $kode_kepemilikan)
    {
        ini_set("pcre.backtrack_limit", "5000000");
        if($bidang_barang == "A") {
            $data = Kib_awal::select('nama_barang', 'kode_64', 'kode_108', 'no_register', 'luas_tanah', 'tahun_pengadaan', 'merk_alamat', 'status_tanah', 'tgl_sertifikat', 'no_sertifikat', 'penggunaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.1%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "B") {
            $data = Kib_awal::select('no_register', 'kode_64', 'kode_108', 'nama_barang','merk_alamat', 'tipe', 'ukuran', 'cc', 'bahan', 'tahun_pengadaan', 'baik', 'kb', 'rb', 'no_rangka_seri', 'no_mesin', 'nopol', 'no_bpkb', 'saldo_barang', 'satuan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.2%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "C") {
            $data = Kib_awal::select('nama_barang', 'kode_64', 'kode_108', 'no_register', 'baik', 'kb', 'rb', 'jumlah_lantai', 'bahan', 'konstruksi', 'luas_lantai', 'merk_alamat','tgl_sertifikat', 'no_sertifikat', 'luas_bangunan', 'status_tanah', 'no_regs_induk_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.3%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "D") {
            $data = Kib_awal::select('nama_barang', 'kode_64', 'kode_108', 'no_register', 'konstruksi', 'tahun_pengadaan', 'panjang_tanah', 'lebar_tanah', 'luas_bangunan', 'merk_alamat', 'tgl_sertifikat', 'no_sertifikat', 'status_tanah', 'no_regs_induk_tanah', 'harga_total_plus_pajak_saldo', 'baik', 'kb', 'rb', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.4%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "E") {
            $data = Kib_awal::select('nama_barang', 'kode_64', 'kode_108', 'no_register', 'merk_alamat', 'tipe', 'bahan', 'ukuran', 'saldo_barang', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.5%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')->get()
                ->toArray();
        } else if($bidang_barang == "F") {
            $data = Kib_awal::select('nama_barang', 'no_register', 'konstruksi', 'jumlah_lantai', 'bahan', 'luas_bangunan', 'merk_alamat', 'tgl_sertifikat', 'no_sertifikat', 'status_tanah', 'harga_total_plus_pajak_saldo', 'nilai_lunas', 'keterangan')
                ->where('kode_108', 'like', '1.3.6%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        }else {
            $data = Kib_awal::select('nama_barang', 'kode_64', 'kode_108', 'no_register', 'luas_tanah', 'tahun_pengadaan', 'merk_alamat', 'status_tanah', 'tgl_sertifikat', 'no_sertifikat', 'penggunaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.$bidang_barang.'%')
                ->where('kode_108', 'like', '1.3.1%')
                ->where("nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kode_kepemilikan', $kode_kepemilikan)
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();

        }

        $asets = array();
        $total_harga = 0;
        $total_nilai_lunas = 0;
        foreach ($data as $value) {
            $aset = $value;
            // $kode_64 = (string)$value['kode_64'].''.PHP_EOL.''.$value['kode_108'];
            $no_register = (string)$value['no_register'];

            if($bidang_barang == "A"){
                $kode_64 = (string)$value['kode_64'].'<br>'.PHP_EOL.''.$value['kode_108'];
                $aset['kode_64'] = $kode_64;
                $aset['no_register'] = $no_register;
            } else if($bidang_barang == "B"){
                $kode_64 = (string)$value['kode_64'].'<br>'.PHP_EOL.''.$value['kode_108'];
                $new_no_register = $no_register.'<br>'.PHP_EOL.''.$kode_64;
                $aset['no_register'] = $new_no_register;

                $merk_tipe = (string)$value['merk_alamat'].''.PHP_EOL.''.$value['tipe'];
                $aset['merk_alamat'] = $merk_tipe;

                $ukuran_cc = (string)$value['ukuran'].''.PHP_EOL.''.$value['cc'];
                $aset['ukuran'] = $ukuran_cc;

                $baik = (string)$value['baik'];
                $kurang_baik = (string)$value['kb'];
                $rusak_berat = (string)$value['rb'];

                if($baik> 0 && $baik>$kurang_baik && $baik>$rusak_berat){
                    $kondisi = "Baik";
                } else if($kurang_baik>0 && $kurang_baik>$baik && $kurang_baik>$rusak_berat){
                    $kondisi = "Kurang Baik";
                } else if($rusak_berat>0 && $rusak_berat>$baik && $rusak_berat>$kurang_baik){
                    $kondisi = "Rusak Berat";
                }

                $aset['baik'] = $kondisi;

                $jumlah_barang_satuan = (string)$value['saldo_barang'].''.PHP_EOL.''.$value['satuan'];
                $aset['saldo_barang'] = $jumlah_barang_satuan;
            } else if($bidang_barang == "C"){
                $kode_64 = (string)$value['kode_64'].'<br>'.PHP_EOL.''.$value['kode_108'];
                $aset['kode_64'] = $kode_64;
                $aset['no_register'] = $no_register;

                $baik = (string)$value['baik'];
                $kurang_baik = (string)$value['kb'];
                $rusak_berat = (string)$value['rb'];
                if($baik> 0 && $baik>$kurang_baik && $baik>$rusak_berat){
                    $kondisi = "Baik";
                } else if($kurang_baik>0 && $kurang_baik>$baik && $kurang_baik>$rusak_berat){
                    $kondisi = "Kurang Baik";
                } else if($rusak_berat>0 && $rusak_berat>$baik && $rusak_berat>$kurang_baik){
                    $kondisi = "Rusak Berat";
                }
                $aset['baik'] = $kondisi;

                $bahan_konstruksi = (string)$value['bahan'].'<br>'.PHP_EOL.''.$value['konstruksi'];
                $aset['bahan'] = $bahan_konstruksi;
            } else if($bidang_barang == "D"){
                $kode_64 = (string)$value['kode_64'].'<br>'.PHP_EOL.''.$value['kode_108'];
                $aset['kode_64'] = $kode_64;
                $aset['no_register'] = $no_register;

                $konstruksi_tahun = (string)$value['konstruksi'].'<br>'.PHP_EOL.''.$value['tahun_pengadaan'];
                $aset['konstruksi'] = $konstruksi_tahun;

                $baik = (string)$value['baik'];
                $kurang_baik = (string)$value['kb'];
                $rusak_berat = (string)$value['rb'];
                if($baik> 0 && $baik>$kurang_baik && $baik>$rusak_berat){
                    $kondisi = "Baik";
                } else if($kurang_baik>0 && $kurang_baik>$baik && $kurang_baik>$rusak_berat){
                    $kondisi = "Kurang Baik";
                } else if($rusak_berat>0 && $rusak_berat>$baik && $rusak_berat>$kurang_baik){
                    $kondisi = "Rusak Berat";
                }
                $aset['baik'] = $kondisi;
            } else if($bidang_barang == 'E'){
                $kode_64 = (string)$value['kode_64'].'<br>'.PHP_EOL.''.$value['kode_108'];
                $aset['kode_64'] = $kode_64;
                $aset['no_register'] = $no_register;
            } else if($bidang_barang == "F"){
                $aset['no_register'] = $no_register;

                $total_nilai_lunas_tmp = $value["nilai_lunas"];
                $total_nilai_lunas+=$total_nilai_lunas_tmp;
                $total_nilai_lunas = $total_nilai_lunas;
            } else {
                $kode_64 = (string)$value['kode_64'].'<br>'.PHP_EOL.''.$value['kode_108'];
                $aset['kode_64'] = $kode_64;
                $aset['no_register'] = $no_register;
            }

            $total_harga_tmp = $value["harga_total_plus_pajak_saldo"];
            $total_harga+=$total_harga_tmp;

            array_push($asets, $aset);
        }

        $data = collect($asets);

        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data->toArray();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["kode_kepemilikan"] = $kode_kepemilikan;
        $data["nama_lokasi"] = $nama_lokasi;
        $data["bidang_barang"] = $bidang_barang;

        $data["nama_jurnal"] = "Saldo Awal KIB";

        $nama_file = "Saldo Awal KIB " . $bidang_barang . " " . $nama_lokasi." ".$this->tahun_sekarang;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.laporanSaldoAwalKib', ['data'=>$data, 'nama_lokasi'=>$nama_lokasi, 'bidang_barang'=>$bidang_barang, 'kode_kepemilikan'=>$kode_kepemilikan]);

        return $pdf->stream($nama_file.$format_file);
    }

    public function exportLaporanKibAwalInv($nomor_lokasi, $kode_kepemilikan)
    {
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '-1');
        ini_set("pcre.backtrack_limit", "1000000000");

        $export = array();
        $data = Kib_awal::select('kamus_lokasis.nomor_lokasi', 'kib_awals.*')
                ->join('kamus_lokasis', 'kib_awals.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->where("kib_awals.nomor_lokasi", 'like', $nomor_lokasi . '%')
                ->orderBy('kib_awals.no_register', 'ASC')
                ->get()
                ->toArray();

        $i= 0;
        $no = '1';
        foreach ($data as $value) {
            $lokasi_asal = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $value["nomor_lokasi"])->first();

            if(!empty($lokasi_asal)) {
                $nama_lokasi = $lokasi_asal->nama_lokasi;
            } else {
                $nama_lokasi = "";
            }

            if ($value["rb"] > 0) {
                $keterangan = "Rusak Berat";
            }
            else{
                $keterangan = "Baik";
            }

            $nomor = $no++;

            $export[$i++] =
                array(
                    "no." => $nomor,
                    "no_register" => $value["no_register"]."<br>".PHP_EOL.$value["kode_64"]."<br>".PHP_EOL.$value["kode_108"],
                    "nama_barang" => $value["nama_barang"],
                    "merk_alamat" => $value["merk_alamat"]."<br>".PHP_EOL.$value["tipe"],
                    "no_sertifikat" => $value["no_sertifikat"]."<br>".PHP_EOL.$value["no_rangka_seri"],
                    "no_mesin" => $value["no_mesin"]."<br>".PHP_EOL.$value["nopol"],
                    "bahan" => $value["bahan"],
                    "tahun_pengadaan" => $value["tahun_pengadaan"],
                    "konstruksi" => $value["konstruksi"]."<br>".PHP_EOL.$value["ukuran"],
                    "satuan" => $value["satuan"],
                    "baik" => $keterangan,
                    "saldo_barang" => $value["saldo_barang"],
                    "harga_total_plus_pajak_saldo" => $value["harga_total_plus_pajak_saldo"],
                    "keterangan" => $value["keterangan"],
                );
        }

        array_multisort(array_column($export, 'no_register'), SORT_ASC, $export);
        $data = collect($export);

        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data->toArray();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["kode_kepemilikan"] = $kode_kepemilikan;
        $data["nama_lokasi"] = $nama_lokasi;

        $data["nama_jurnal"] = "Buku Inventaris";

        $nama_file = "Laporan Kib Awal Saldo Inventaris " . $nama_lokasi." ".$this->tahun_sekarang;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.laporanSaldoAwalInv', ['data'=>$data, 'nama_lokasi'=>$nama_lokasi, 'kode_kepemilikan'=>$kode_kepemilikan]);

        return $pdf->stream($nama_file.$format_file);

        // return $data;
    }

    public function exportLaporanRekapPerolehan()
    {
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '-1');
        ini_set("pcre.backtrack_limit", "1000000000");

        $rekap = array();
        $i = 0;

        $daftar_sub_unit = Kamus_sub_unit::select('nomor_sub_unit', 'nama_sub_unit')->get();

        foreach ($daftar_sub_unit as $value) {
            $kode_108 = array('1.3.1', '1.3.2', '1.3.3', '1.3.4', '.1.3.5', '1.3.6', '1.5.3', '1.5.4');
            $kib_a = 0;
            $kib_b = 0;
            $kib_c = 0;
            $kib_d = 0;
            $kib_e = 0;
            $kib_f = 0;
            $kib_g = 0;
            $kib_r = 0;

            foreach ($kode_108 as $kib) {
                $data = Kib::select('harga_total_plus_pajak_saldo')
                ->where('nomor_lokasi', 'like', $value['nomor_sub_unit'] . '%')
                ->where('kode_108', 'like', $kib . '%')
                ->where('saldo_barang', '>', 0)
                ->get()
                ->toArray();


                foreach($data as $aset) {
                    if($kib == '1.3.1') {
                        $kib_a += $aset['harga_total_plus_pajak_saldo'];
                    } else if($kib == '1.3.2') {
                        $kib_b += $aset['harga_total_plus_pajak_saldo'];
                    } else if($kib == '1.3.3') {
                        $kib_c += $aset['harga_total_plus_pajak_saldo'];
                    } else if($kib == '1.3.4') {
                        $kib_d += $aset['harga_total_plus_pajak_saldo'];
                    } else if($kib == '1.3.5') {
                        $kib_e += $aset['harga_total_plus_pajak_saldo'];
                    } else if($kib == '1.3.6') {
                        $kib_f += $aset['harga_total_plus_pajak_saldo'];
                    } else if($kib == '1.5.3') {
                        $kib_g += $aset['harga_total_plus_pajak_saldo'];
                    } else if($kib == '1.5.4') {
                        $kib_r += $aset['harga_total_plus_pajak_saldo'];
                    }
                }
            }

            $rekap[$i++] = array(
                'nama_sub_unit' => $value['nama_sub_unit'],
                'kib_a' => $kib_a,
                'kib_b' => $kib_b,
                'kib_c' => $kib_c,
                'kib_d' => $kib_d,
                'kib_e' => $kib_e,
                'kib_f' => $kib_f,
                'kib_g' => $kib_g,
                'kib_r' => $kib_r
            );
        }

        // var_dump($rekap);
        // die;

        $data = collect($rekap);
        // return $export;

        $data->toArray();

        $data["nama_jurnal"] = "Rekap Laporan Nilai Perolehan Semua";

        $nama_file = "Rekap Perolehan Semua";
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.laporanRekapPerolehan', ['data'=>$data]);

        return $pdf->stream($nama_file.$format_file);

        // return $data;
    }

    public function exportLaporanRekapPenyusutan()
    {
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '-1');
        ini_set("pcre.backtrack_limit", "1000000000");

        $rekap = array();
        $i = 0;

        $daftar_sub_unit = Kamus_sub_unit::select('nomor_sub_unit', 'nama_sub_unit')->get();

        foreach ($daftar_sub_unit as $unit) {
            $daftar_kib = array('1.3.1', '1.3.2', '1.3.3', '1.3.4', '.1.3.5', '1.3.6', '1.5.3', '1.5.4');
            $penyusutan_kib_a = 0;
            $penyusutan_kib_b = 0;
            $penyusutan_kib_c = 0;
            $penyusutan_kib_d = 0;
            $penyusutan_kib_e = 0;
            $penyusutan_kib_f = 0;
            $penyusutan_kib_g = 0;
            $penyusutan_kib_r = 0;

            foreach ($daftar_kib as $kib) {
                $data = Kib::where('nomor_lokasi', 'like', $unit['nomor_sub_unit'] . '%')
                ->where('kode_108', 'like', $kib . '%')
                ->where('saldo_barang', '>', 0)
                ->get()
                ->toArray();

                $j = 0;
                $jumlah_barang = 0;
                $tahun_acuan = date('Y')-1;
                $masa_terpakai;
                $total_nilai_perolehan = 0;
                $total_akumulasi_penyusutan = 0;
                $total_akumulasi_penyusutan_berjalan = 0;
                $total_beban = 0;
                $total_nilai_buku = 0;
                $total_rehab = 0;
                $jumlah_rehab = 0;

                foreach($data as $value) {
                    if(array_key_exists('kode_64', $value)) {
                        if(is_null($value['kode_64'])) {
                            $mm = null;
                        } else {
                            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
                        }
                    } else {
                        $mm = NULL;
                    }

                    $saldo_kosong = false;
                    $aset_induk = false;
                    $aset_rehab = false;
                    $kib_e = false;

                    if($value["saldo_barang"] == 0) {
                        $saldo_kosong = true;
                    }

                    $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
                    $sub_64 = substr($value["kode_64"], 0, 8);

                    if(in_array($sub_64, $kode_e)) {
                        if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                            $kib_e = false;
                        } else {
                            $kib_e = true;
                        }
                    }

                    $pakai_habis = false;
                    $ekstrakom = false;

                    if($value["pos_entri"] == "PAKAI_HABIS") {
                        $pakai_habis = true;
                    }

                    if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                        $ekstrakom = true;
                    }

                    $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
                    $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

                    if(!is_null($induk)) {
                        $aset_induk = true;
                    }

                    if(!is_null($anak)) {
                        $aset_rehab = true;
                    }

                    if($saldo_kosong || $aset_rehab || $pakai_habis || $ekstrakom) {
                        continue;
                    }

                    if(!is_null($mm)) {
                        if($mm->masa_manfaat == 0) {
                            $masa_manfaat = 0;
                            $mm_induk = 0;
                        } else {
                            $masa_manfaat = (int)$mm->masa_manfaat;
                            $mm_induk = (int)$mm->masa_manfaat;
                        }
                    } else {
                        if($this->bidang_barang == "A") {
                            $masa_manfaat = 0;
                            $mm_induk = 0;
                        } else if($this->bidang_barang == "B") {
                            $masa_manfaat = 5;
                            $mm_induk = 5;
                        } else if($this->bidang_barang == "C" || $this->bidang_barang == "D") {
                            $masa_manfaat = 50;
                            $mm_induk = 50;
                        } else if($this->bidang_barang == "G") {
                            $masa_manfaat = 4;
                            $mm_induk = 4;
                        } else if(strpos($this->bidang_barang, 'E') !== false) {
                            $masa_manfaat = 5;
                            $mm_induk = 5;
                        }
                    }

                    if($masa_manfaat == 0) {
                        if($value['kode_108'] == '1.5.4.01.01.01.002') {
                            $masa_manfaat = 5;
                            $mm_induk = 5;
                        } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                            $masa_manfaat = 50;
                            $mm_induk = 50;
                        } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                            $masa_manfaat = 40;
                            $mm_induk = 40;
                        } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                            $masa_manfaat = 5;
                            $mm_induk = 5;
                        }
                    }

                    if($aset_induk) {
                        $rehabs = Rehab::join('kibs', 'kibs.id_aset', '=', 'rehabs.rehab_id')
                        ->select('rehabs.rehab_id', 'kibs.tahun_pengadaan as tahun_rehab', 'kibs.harga_total_plus_pajak_saldo as nilai_rehab', 'kibs.kode_108 as kode_rek_rehab', 'rehabs.tambah_manfaat')
                        ->where('rehabs.aset_induk_id', $value['id_aset'])
                        ->orderBy('kibs.tahun_pengadaan', 'asc')->get();


                        if(!empty($rehabs)) {
                            $detail_penyusutan = $rehabs->toArray();
                        }

                        $count = sizeof($detail_penyusutan);

                        $status_aset = 1;

                        $jumlah_barang_tmp = $value["saldo_barang"];
                        $tahun_pengadaan_tmp = intval($value["tahun_pengadaan"]);
                        $nilai_pengadaan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                        $persen = 0;
                        $penambahan_nilai = 0;
                        $masa_tambahan = 0;
                        $masa_terpakai_tmp = 0;
                        $sisa_masa_tmp = $masa_manfaat;
                        $penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
                        $akumulasi_penyusutan_tmp = 0;
                        $akumulasi_penyusutan_berjalan_tmp = 0;
                        $nilai_buku_tmp = 0;
                        $index = 0;
                        $k = 0;

                        for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
                            $tahun_rehab = intval($detail_penyusutan[$index]["tahun_rehab"]);
                            if($th == $tahun_rehab) {
                                $jumlah_renov_tahunx = 0;
                                $tahun_pembanding = 0;

                                if($tahun_pengadaan_tmp == $tahun_rehab) {
                                    $nilai_buku_tmp = $nilai_pengadaan_tmp;
                                }

                                for ($x = 0; $x < $count; $x++) {
                                     $tahun_pembanding = intval($detail_penyusutan[$x]["tahun_rehab"]);

                                     if($tahun_pembanding === $tahun_rehab) {
                                        $jumlah_renov_tahunx++;
                                     }
                                }

                                if($jumlah_renov_tahunx == 1) {
                                    $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;

                                    $persen = (int)$persen;

                                    $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                    if(is_null($kode_64)) {
                                        $kode_108 = substr($kode_108, 0, 14);
                                        $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                        if(is_null($kode_64)) {
                                            $kode_108 = substr($kode_108, 0, 11);
                                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                        } else {
                                            $kode_64 = $kode_64->kode_64;
                                        }
                                    } else {
                                        $kode_64 = $kode_64->kode_64;
                                    }

                                    $kode_64 = substr($kode_64, 0, 8);
                                    $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                                    if(!is_null($masa_tambah)) {
                                        $masa_tambahan = $masa_tambah->masa_tambahan;
                                    }

                                    if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                        $sisa_masa_tmp += $masa_tambahan;
                                        if($sisa_masa_tmp > $mm_induk) {
                                            $masa_manfaat = $mm_induk;
                                            $sisa_masa_tmp = $mm_induk;
                                        } else {
                                            $masa_manfaat = $sisa_masa_tmp;
                                        }
                                        $masa_terpakai_tmp = 1;
                                    } else {
                                        ++$masa_terpakai_tmp;
                                    }

                                    $nilai_buku_tmp += $detail_penyusutan[$index]["nilai_rehab"];

                                    if($sisa_masa_tmp > 0) {
                                        $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                                    } else {
                                        $penyusutan_per_tahun_tmp = 0;
                                    }

                                    $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                                    $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                                    --$sisa_masa_tmp;
                                    $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilai_rehab"];
                                    $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                                    if($index < $count-1) {
                                        $index++;
                                    }
                                } else {
                                    $penambahan_nilai = 0;
                                    for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
                                        $penambahan_nilai += $detail_penyusutan[$index]["nilai_rehab"];
                                        if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
                                            $index++;
                                        }
                                    }

                                    $persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;

                                    $persen = (int)$persen;
                                    $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                    if(is_null($kode_64)) {
                                        $kode_108 = substr($kode_108, 0, 14);
                                        $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                        if(is_null($kode_64)) {
                                            $kode_108 = substr($kode_108, 0, 11);
                                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                        } else {
                                            $kode_64 = $kode_64->kode_64;
                                        }
                                    } else {
                                        $kode_64 = $kode_64->kode_64;
                                    }

                                    $kode_64 = substr($kode_64, 0, 8);
                                    $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                                    if(!is_null($masa_tambah)) {
                                        $masa_tambahan = $masa_tambah->masa_tambahan;
                                    }

                                    if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                        $sisa_masa_tmp += $masa_tambahan;
                                        if($sisa_masa_tmp > $mm_induk) {
                                            $masa_manfaat = $mm_induk;
                                            $sisa_masa_tmp = $mm_induk;
                                        } else {
                                            $masa_manfaat = $sisa_masa_tmp;
                                        }
                                        $masa_terpakai_tmp = 1;
                                    } else {
                                        ++ $masa_terpakai_tmp;
                                    }

                                    $nilai_buku_tmp += $penambahan_nilai;
                                    $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                                    $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                                    $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                                    $nilai_pengadaan_tmp += $penambahan_nilai;

                                    $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;
                                    if($sisa_masa_tmp > 0) {
                                        --$sisa_masa_tmp;
                                    }

                                    if($index < $count-1) {
                                        $index++;
                                    }
                                }
                            } else {
                                ++$masa_terpakai_tmp;

                                if($sisa_masa_tmp > 0) {
                                    --$sisa_masa_tmp;
                                }

                                $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                                $nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;
                                $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                                if($masa_terpakai_tmp > $masa_manfaat) {
                                    $akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
                                    $akumulasi_penyusutan_berjalan_tmp = $nilai_pengadaan_tmp;
                                    $nilai_buku_tmp = 0;
                                    $sisa_masa_tmp = 0;
                                }
                            }
                        }

                        $akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;
                        $jumlah_barang += $jumlah_barang_tmp;

                        if($this->bidang_barang == "A" || $kib_e == true) {
                            $akumulasi_penyusutan_tmp = 0;
                            $akumulasi_penyusutan_berjalan_tmp = 0;
                            $penyusutan_per_tahun_tmp = 0;
                            $nilai_buku_tmp = $nilai_pengadaan_tmp;
                            $akumulasi_penyusutan = 0;
                            $beban = 0;
                            $nilai_buku = $nilai_pengadaan_tmp;
                        }

                        if($kib == '1.3.1') {
                            $penyusutan_kib_a += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.2') {
                            $penyusutan_kib_b += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.3') {
                            $penyusutan_kib_c += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.4') {
                            $penyusutan_kib_d += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.5') {
                            $penyusutan_kib_e += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.3.6') {
                            $penyusutan_kib_f += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.5.3') {
                            $penyusutan_kib_g += $akumulasi_penyusutan_berjalan_tmp;
                        } else if($kib == '1.5.4') {
                            $penyusutan_kib_r += $akumulasi_penyusutan_berjalan_tmp;
                        }
                    } else {
                        $masa_terpakai = $tahun_acuan - $value["tahun_pengadaan"] + 1;
                        $masa_sisa = $masa_manfaat - $masa_terpakai;
                        $nilai_perolehan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                        $status_aset = 0;
                        $jumlah_barang_tmp = $value["saldo_barang"];

                        if($masa_terpakai > $masa_manfaat) {
                            $nilai_perolehan = $nilai_perolehan_tmp;
                            $akumulasi_penyusutan = $nilai_perolehan_tmp;
                            $akumulasi_penyusutan_berjalan = $nilai_perolehan_tmp;
                            $masa_sisa = 0;
                            $beban = 0;
                            $nilai_buku = 0;
                        } else {
                            $nilai_perolehan = $nilai_perolehan_tmp;
                            $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
                            $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan_tmp;
                            $beban = 1/$masa_manfaat*$nilai_perolehan_tmp;
                            $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
                        }

                        if($this->bidang_barang == "A" || $kib_e == true) {
                            $akumulasi_penyusutan = 0;
                            $akumulasi_penyusutan_berjalan = 0;
                            $beban = 0;
                            $nilai_buku = $nilai_perolehan_tmp;
                        }

                        if($kib == '1.3.1') {
                            $penyusutan_kib_a += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.2') {
                            $penyusutan_kib_b += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.3') {
                            $penyusutan_kib_c += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.4') {
                            $penyusutan_kib_d += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.5') {
                            $penyusutan_kib_e += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.3.6') {
                            $penyusutan_kib_f += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.5.3') {
                            $penyusutan_kib_g += $akumulasi_penyusutan_berjalan;
                        } else if($kib == '1.5.4') {
                            $penyusutan_kib_r += $akumulasi_penyusutan_berjalan;
                        }
                    }

                }
            }

            $rekap[$i++] = array(
                'nama_sub_unit' => $unit['nama_sub_unit'],
                'penyusutan_kib_a' => $penyusutan_kib_a,
                'penyusutan_kib_b' => $penyusutan_kib_b,
                'penyusutan_kib_c' => $penyusutan_kib_c,
                'penyusutan_kib_d' => $penyusutan_kib_d,
                'penyusutan_kib_e' => $penyusutan_kib_e,
                'penyusutan_kib_f' => $penyusutan_kib_f,
                'penyusutan_kib_g' => $penyusutan_kib_g,
                'penyusutan_kib_r' => $penyusutan_kib_r
            );
        }

        // var_dump($rekap);
        // die;

        $data = collect($rekap);

        $data->toArray();

        $data["nama_jurnal"] = "Rekap Laporan Penyusutan Semua";

        $nama_file = "Rekap Penyusutan Semua";
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.laporanRekapPenyusutan', ['data'=>$data]);

        return $pdf->stream($nama_file.$format_file);

        // return $data;
    }

    public function exportLaporanKibGKabupaten()
    {   
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '-1');
        ini_set("pcre.backtrack_limit", "1000000000");

        $data = Kib::where('bidang_barang', 'like', '%G%')
                ->where('saldo_barang', '>', 0)
                ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tipe', 'kibs.saldo_barang', 'kibs.satuan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.tahun_pengadaan')
                ->orderBy('kibs.tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();

        $asets = array();

        $asets = array();
        foreach ($data as $value) {
            $aset = $value;
            $no_register = (string)$value['no_register'] . ' ';
            $aset['no_register'] = $no_register;

            array_push($asets, $aset);
        }

        $nama_kabupaten = Kamus_kab_kota::select('nama_kab')->first()->nama_kab;

        $data = collect($asets);
        // print_r($data->toArray());
        $data->toArray();

        $nama_file = "Rekap Laporan Aset Tak Berwujud ".$this->tahun_sekarang. " Kabupaten ".$nama_kabupaten;;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.laporanKibG', ['data'=>$data, 'nama_kabupaten'=>$nama_kabupaten]);

        return $pdf->stream($nama_file.$format_file);

        // return $data;
    }

    public function exportLaporanAsetLain()
    {   
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '-1');
        ini_set("pcre.backtrack_limit", "1000000000");

        $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','ukuran','cc','bahan','tahun_pengadaan','baik','kb', 'rb', 'no_rangka_seri', 'no_mesin', 'nopol', 'no_bpkb', 'saldo_barang', 'satuan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('kode_108', 'like', '1.5.5%')
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();

        $asets = array();

        $asets = array();
        foreach ($data as $value) {
            $aset = $value;
            $no_register = (string)$value['no_register'] . ' ';
            $aset['no_register'] = $no_register;

            array_push($asets, $aset);
        }

        $nama_kabupaten = Kamus_kab_kota::select('nama_kab')->first()->nama_kab;

        $data = collect($asets);
        // print_r($data->toArray());
        $data->toArray();
        
        $nama_file = "Rekap Laporan Aset Lain-lain ".$this->tahun_sekarang. " Kabupaten ".$nama_kabupaten;;
        $format_file = ".pdf";

            $pdf = MPDF::loadView('html.KIB.laporanAsetLain', ['data'=>$data, 'nama_kabupaten'=>$nama_kabupaten]);

            return $pdf->stream($nama_file.$format_file);

        // return $data;
    }

    public function exportLaporanAsetTetapKabupaten($bidang_barang)
    {   
        //require_once APPPATH.'libraries/Mpdf/autoload.php';
        ini_set('max_execution_time', 1800);
        ini_set("memory_limit","-1");
        ini_set("pcre.backtrack_limit", "1000000000");

        if($bidang_barang == "A") {
            $data = Kib::select('kode_108', 'no_register', 'nama_barang', 'merk_alamat', 'luas_tanah', 'tahun_pengadaan', 'tgl_sertifikat', 'no_sertifikat', 'penggunaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.strtoupper($bidang_barang).'%')
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "B") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','ukuran','cc','bahan','tahun_pengadaan','baik','kb', 'rb', 'no_rangka_seri', 'no_mesin', 'nopol', 'no_bpkb', 'saldo_barang', 'satuan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.strtoupper($bidang_barang).'%')
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "C") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','jumlah_lantai','luas_lantai','no_imb', 'tgl_imb', 'luas_bangunan', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.strtoupper($bidang_barang).'%')
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "D") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','panjang_tanah','lebar_tanah','luas_tanah','no_imb', 'tgl_imb', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.strtoupper($bidang_barang).'%')
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else if($bidang_barang == "E") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','jumlah_lantai','luas_lantai','no_imb', 'tgl_imb', 'luas_bangunan', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.strtoupper($bidang_barang).'%')
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')->get()
                ->toArray();
        } else if($bidang_barang == "F") {
            $data = Kib::select('kode_108','no_register','nama_barang','merk_alamat','baik','kb','rb','konstruksi','bahan','jumlah_lantai','luas_lantai','no_imb', 'tgl_imb', 'luas_bangunan', 'status_tanah', 'tahun_pengadaan', 'harga_total_plus_pajak_saldo', 'keterangan')
                ->where('bidang_barang', 'like', '%'.strtoupper($bidang_barang).'%')
                ->where('saldo_barang', '>', 0)
                ->orderBy('tahun_pengadaan', 'ASC')
                ->get()
                ->toArray();
        } else {
            return response()->json(['errors' => 'data  untuk KIB ' .strtoupper($bidang_barang). ' tidak tersedia']);
        }

        $asets = array();

        $asets = array();
        foreach ($data as $value) {
            $aset = $value;
            $no_register = (string)$value['no_register'] . ' ';
            $aset['no_register'] = $no_register;

            array_push($asets, $aset);
        }

        $nama_kabupaten = Kamus_kab_kota::select('nama_kab')->first()->nama_kab;

        $data = collect($asets);
        // print_r($data->toArray());
        $data->toArray();

        $nama_file = "Rekap Laporan Aset Tetap KIB ".strtoupper($bidang_barang). ' '.$this->tahun_sekarang. " Kabupaten ".$nama_kabupaten;;
        $format_file = ".pdf";

        $pdf = MPDF::loadView('html.KIB.laporanAsetTetap', ['data'=>$data, 'nama_kabupaten'=>$nama_kabupaten, 'bidang_barang'=>$bidang_barang]);

        return $pdf->stream($nama_file.$format_file);

        // return $data;
    }
}
