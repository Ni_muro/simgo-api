<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
			section {
				max-width: auto;
				margin: 0 auto;
			}

			body{
				font-family: "Calibri", Helvetica, Arial, sans-serif;
			}

            table.footer{
                width: 100%;
				background-color: none;
            }
            table.footer tr{
                border:none;
            }
            table.footer td.spasi{
                width: 70%;
            }

			table {
				width: 100%;
				border-collapse: collapse;
				font-family: "Calibri", Helvetica, Arial, sans-serif;
				background-color: none;
			}

			.table--striped tr:nth-of-type(odd) {
				background-color: #F9F9F9;
			}

			.table--bordered tr {
				border-bottom: 1px solid black;
			}

			th {
				border-top: 1px solid black;
				font-weight: bold;
				font-size: 11px;
				border-bottom: 1px solid black;
				text-align: center;
                color: black;
			}

			td {
				font-size: 10px;
				padding: 2px;
				text-align: left;
				text-overflow: ellipsis;
				color: black;
				line-height: 1.5em;
				border-top: 1px solid black;
			}

			tbody tr:first-child {
				border-top: 0;
			}

            @page {
                footer: page-footer;
            }

	</style>
</head>
<body>
	<center>
		<h2 style="text-align: center;">{{ $data['nama_jurnal'] }}</h2>
		<h3 style="margin-top: -1%; text-align: center;">{{date('Y')-1}}</h3>
	</center>

	<section style="margin-top: 2%;">
        <div class="table-responsive">
        <table class="table--hover" style="text-align: center; justify-content: center;">
            <thead>
                <tr>
                    <th>NO. </th>
                    <th style="width:20%;">SKPD</th>
                    <th>Penyusutan KIB A</th>
                    <th>Penyusutan KIB B</th>
                    <th>Penyusutan KIB C</th>
                    <th>Penyusutan KIB D</th>
                    <th>Penyusutan KIB E</th>
                    <th>Penyusutan KIB F</th>
                    <th>Penyusutan KIB G</th>
                    <th>Penyusutan KIB R</th>
                </tr>
                <tr>
                    <th>1</th>
                    <th style="width:20%;">2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                </tr>
            </thead>
            <tbody>
                @php $x=1 @endphp
                @php
                    $kib_a=0;
                    $kib_b=0;
                    $kib_c=0;
                    $kib_d=0;
                    $kib_e=0;
                    $kib_f=0;
                    $kib_g=0;
                    $kib_r=0;
                @endphp
                @foreach($data as $dt)
                @if(isset($dt['nama_sub_unit']))
                    <tr>
                        <td >{{$x++}}</td>
                        <td style="width:20%;">{{$dt['nama_sub_unit']}}</td>
                        <td>Rp. {{number_format($dt['penyusutan_kib_a'], 2, ',','.')}}</td>
                        <td>Rp. {{number_format($dt['penyusutan_kib_b'], 2, ',','.')}}</td>
                        <td>Rp. {{number_format($dt['penyusutan_kib_c'], 2, ',','.')}}</td>
                        <td>Rp. {{number_format($dt['penyusutan_kib_d'], 2, ',','.')}}</td>
                        <td>Rp. {{number_format($dt['penyusutan_kib_e'], 2, ',','.')}}</td>
                        <td>Rp. {{number_format($dt['penyusutan_kib_f'], 2, ',','.')}}</td>
                        <td>Rp. {{number_format($dt['penyusutan_kib_g'], 2, ',','.')}}</td>
                        <td>Rp. {{number_format($dt['penyusutan_kib_r'], 2, ',','.')}}</td>
                        @php
                            $kib_a += $dt['penyusutan_kib_a'];
                            $kib_b += $dt['penyusutan_kib_b'];
                            $kib_c += $dt['penyusutan_kib_c'];
                            $kib_d += $dt['penyusutan_kib_d'];
                            $kib_e += $dt['penyusutan_kib_e'];
                            $kib_f += $dt['penyusutan_kib_f'];
                            $kib_g += $dt['penyusutan_kib_g'];
                            $kib_r += $dt['penyusutan_kib_r'];
                        @endphp
                    </tr>
                @endif
                @endforeach
                <tr>
                    <td colspan="2" style="text-align: right; color: black; border-bottom: 1px solid black;">Total Nilai (Rp.) : </td>
                    <td style="color: black; border-bottom: 1px solid black;">Rp. {{number_format($kib_a, 2, ',','.')}}</td>
                    <td style="color: black; border-bottom: 1px solid black;">Rp. {{number_format($kib_b, 2, ',','.')}}</td>
                    <td style="color: black; border-bottom: 1px solid black;">Rp. {{number_format($kib_c, 2, ',','.')}}</td>
                    <td style="color: black; border-bottom: 1px solid black;">Rp. {{number_format($kib_d, 2, ',','.')}}</td>
                    <td style="color: black; border-bottom: 1px solid black;">Rp. {{number_format($kib_e, 2, ',','.')}}</td>
                    <td style="color: black; border-bottom: 1px solid black;">Rp. {{number_format($kib_f, 2, ',','.')}}</td>
                    <td style="color: black; border-bottom: 1px solid black;">Rp. {{number_format($kib_g, 2, ',','.')}}</td>
                    <td style="color: black; border-bottom: 1px solid black;">Rp. {{number_format($kib_r, 2, ',','.')}}</td>
                </tr>
            </tbody>
        </table>
        </div>
	  	<br>
        <footer>
            <table class="footer">
                <tr>
                    <td>
                        Mengetahui,
                        <br><br><br><br><span>NIP</span>
                    </td>
                    <td class="spasi"></td>
                    <td>
                        Mojokerto, {{date('d/m/Y')}}
                        <br><br><br><br><span>NIP</span>
                    </td>
                </tr>
            </table>
        </footer>

        <htmlpagefooter name="page-footer">
            <table class="footer">
                <tr>
                    <td>
                        {{ $data['nama_jurnal'] }} {{date('Y')-1}}
                    </td>
                    <td style="text-align: right;">
                        Halaman {PAGENO} / {nbpg}
                    </td>
                </tr>
            </table>
        </htmlpagefooter>
    </section>


</body>
</html>
