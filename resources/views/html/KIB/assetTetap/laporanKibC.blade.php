<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
            section {
                max-width: auto;
                margin: 0 auto;
            }

            body{
                font-family: "Calibri", Helvetica, Arial, sans-serif;
            }

            table.footer{
                width: 100%;
                background-color: none;
            }
            table.footer tr{
                border:none;
            }
            table.footer td.spasi{
                width: 70%;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                font-family: "Calibri", Helvetica, Arial, sans-serif;
                background-color: none;
            }

            .table--striped tr:nth-of-type(odd) {
                background-color: #F9F9F9;
            }

            .table--bordered tr {
                border-bottom: 1px solid black;
            }

            th {
                border-top: 1px solid black;
                font-weight: bold;
                font-size: 11px;
                border-bottom: 1px solid black;
                text-align: center;
                color: black;
            }

            td {
                font-size: 10px;
                padding: 2px;
                text-align: left;
                text-overflow: ellipsis;
                color: black;
                line-height: 1.5em;
                border-top: 1px solid black;
            }

            tbody tr:first-child {
                border-top: 0;
            }

            @page {
                footer: page-footer;
            }

    </style>
</head>
<body>
    <center>
        <h2 style="text-align: center;">Laporan Aset Tetap (KIB {{strtoupper($bidang_barang)}})  {{date('Y')-1}}</h2>
        <h3 style="margin-top: -1%; text-align: center;">Kabupaten {{$nama_kabupaten}}</h3>
    </center>

    <section style="margin-top: 2%;">
        <div class="table-responsive">
        <table class="table--hover" style="text-align: center; justify-content: center;">
                <thead>
                    <tr>
                        <th style="width:2%;">NO</th>
                        <th style="width:11.5%;">NOMOR LOKASI</th>
                        <th style="width:8%;">LOKASI</th>
                        <th style="width:10%;">NO REGISTER</th>
                        <th style="width:5.5%;">KODE SUB SUB KELOMPOK</th>
                        <th style="width:5%;">KODE SUB KEL AT</th>
                        <th>RINCIAN OBJECT</th>
                        <th>NAMA BARANG</th>
                        <th>MERK ALAMAT</th>
                        <th style="width:5%;">TIPE</th>
                        <th style="width:3.75%;">KODE RUANGAN</th>
                        <th style="width:3.75%;">SALDO BARANG</th>
                        <th style="width:3.75%;">SALDO GUDANG</th>
                        <th style="width:3.2%;">SATUAN</th>
                        <th style="width:6%;">HARGA TOTAL PLUS PAJAK SALDO</th>
                        <th>TAHUN PENGADAAN</th>
                    </tr>
                    <tr>
                        <th>1</th>
                        <th style="width:11.5%;">2</th>
                        <th style="width:8%;">3</th>
                        <th style="width:10%;">4</th>
                        <th style="width:5.5%;">5</th>
                        <th style="width:5%;">6</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th style="width:6%;">15</th>
                        <th>16</th>
                    </tr>
                </thead>
                <tbody>
                    @php $x=1 @endphp
                    @php $total=0 @endphp
                    @foreach($data as $dt)
                        <tr>
                            <?php 
                                $nama_lokasi = App\Models\Kamus\Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi',$dt['nomor_lokasi'])->first()->nama_lokasi;     

                                $explode = explode(".", $dt['no_key']);
                            ?>

                            <td >{{$x++}}</td>
                            <td style="width:11.5%;">{{$dt['nomor_lokasi']}}</td>
                            <td style="width:8%;">{{$nama_lokasi}}</td>
                            <td style="width:10%;">{{$dt['no_register']}}</td>
                            <td style="width:5.5%;">{{end($explode)}}</td>
                            <td style="width:5%;">{{$dt['kode_64']}}</td>
                            <td >Gedung</td>
                            <td >{{$dt['nama_barang']}}</td>
                            <td >{{$dt['merk_alamat']}}</td>
                            <td >{{$dt['tipe']}}</td>
                            <td >{{$dt['kode_ruangan']}}</td>
                            <td style="text-align: center;">{{$dt['saldo_barang']}}</td>
                            <td style="text-align: center;">{{$dt['saldo_gudang']}}</td>
                            <td style="text-align: center;">{{$dt['satuan']}}</td>
                            <td style="width:6%; text-align: center;">{{number_format($dt['harga_total_plus_pajak'], 2, ',','.')}}</td>
                            <td style="text-align: center;">{{$dt['tahun_pengadaan']}}</td>
                            @php $total += $dt['harga_total_plus_pajak'] @endphp
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="14" style="text-align: right; color: black; border-bottom: 1px solid black;">Total Nilai (Rp.) : </td>
                        <td colspan="2" style="color: black; border-bottom: 1px solid black;">{{number_format($total, 2, ',','.')}}</td>
                    </tr>
                </tbody>
        </table>
        </div>
        <br>
        <footer>
            <table class="footer">
                <tr>
                    <td>
                        Mengetahui,
                        <br><br><br><br><span>NIP</span>
                    </td>
                    <td class="spasi"></td>
                    <td>
                        Mojokerto, {{date('d/m/Y')}}
                        <br><br><br><br><span>NIP</span>
                    </td>
                </tr>
            </table>
        </footer>

        <htmlpagefooter name="page-footer">
            <table class="footer">
                <tr>
                    <td>
                        Laporan Aset Tetap (KIB {{strtoupper($bidang_barang)}}) Kabupaten {{$nama_kabupaten}} {{date('Y')-1}}
                    </td>
                    <td style="text-align: right;">
                        Halaman {PAGENO} / {nbpg}
                    </td>
                </tr>
            </table>
        </htmlpagefooter>
    </section>


</body>
</html>
